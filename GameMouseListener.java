import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class GameMouseListener implements MouseListener{
	FrameG frame;
	
	GameMouseListener(FrameG f) {
		frame = f;
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		int x = e.getX();
		int y = e.getY();
		int squareX = (x - 50) / 30;
		int squareY = (y - 50) / 30;
		frame.map.h.posx = squareX;
		frame.map.h.posy = squareY;
		frame.repaint();
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
