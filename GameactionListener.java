import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//import game.GameFrame;

public class GameactionListener implements ActionListener {
    FrameG frame;
	
	GameactionListener(FrameG f) {
		frame = f;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		frame.button.setVisible(false);
		frame.label.setText("");
	    frame.label1.setText("");
		frame.mapPanel.setVisible(true);
		frame.buttona.setVisible(true);
		frame.buttonb.setVisible(true);
		frame.labeln.setVisible(false);
		frame.mapPanel.setSize(frame.map.sizex * 30 + 1, frame.map.sizey * 30 + 1);
	}
		
	}


